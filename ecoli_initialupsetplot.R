# load libraries
library(tidyverse)
library(kableExtra)
library(janitor)
library(magrittr)

# read in files and parse data
ecoli_allRP2 <- readr::read_csv("../../GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/microreact/ecoli_invcombo.csv")
ecoli_allRP3 <- ecoli_allRP2 %>% left_join(plasmid_profile %>% select(id, plasmid_profile), by = "id")

# making upset plots
library(ghruR)
library(kableExtra)
library(tidyverse)
library(magrittr)
library(ggplot2)
library(ggthemes)
library(ggeasy)
library(forcats)
library(ggupset)

acquired_amr_data <- readr::read_csv("../../GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/AMR/acquired_amr.csv", col_types = cols(.default = "c"))
point_amr_data <- readr::read_csv("../../GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/AMR/point_amr.csv", col_types = cols(.default = "c"))
# select invasives
acquired_amr_data %<>% inner_join(ecoli_allRP3 %>% select(id), by = c("name" = "id")) 
point_amr_data %<>% inner_join(ecoli_allRP3 %>% select(id), by = c("name" = "id"))
# filter acquired amr data
filtered_long_amr_data <-  acquired_amr_data %>%
  tidyr::pivot_longer(-name, names_to = 'metric', values_to = 'value') %>% # so, you have 3 resulting columns namely `Sample id`, metric (which is now the header of the amr column headers that have been converted to rows), and value (header for the former values of the column headers (which has now been converted into rows))
  tidyr::extract(col = 'metric', into = c('ariba_cluster', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE) %>% # the metric column/header is split/divided into two columns (ariba_cluster and metric), and the `content` of the former metric header is split into two using regular expressions 
  tidyr::pivot_wider(names_from = 'metric', values_from = 'value') %>% 
  filter(stringr::str_starts(assembled, "yes")) %>%
  # the column called "assembled" is targeted and the value (content of the assembled column) "yes" or "yes_nonunique" is retained.
  na_if("NA") %>%
  mutate(ctg_cov = as.numeric(ctg_cov)) %>%
  filter(ctg_cov >= 10)
# From these high quality matched we will select the accession from the ref_seq field so that this can be used to annotate the matches with metadata from the NCBI database from which the matches were found
# select the ref_seq column and extract just the accession
filtered_long_amr_data  %<>%
  dplyr::select(name, ref_seq) %>%
  mutate(ref_seq = stringr::str_extract(ref_seq, "NG_.+$")) %>%
  dplyr::rename(refseq_nucleotide_accession = ref_seq)
#Pull in NCBI metadata from using a function from the ghruR library and join this to the filtered amr data
ncbi_acquired_gene_metadata <- readr::read_tsv("https://ftp.ncbi.nlm.nih.gov/pathogen/Antimicrobial_resistance/Data/latest/ReferenceGeneCatalog.txt") %>%
  dplyr::select(refseq_nucleotide_accession, allele, gene_family, class,subclass)
# convert to long format
# annotate amr data with NCBI metadata and merge the allele and gene_family columns since not all gene_families have an allele
annotated_amr_data <- filtered_long_amr_data %>%
  left_join(ncbi_acquired_gene_metadata, by = "refseq_nucleotide_accession") %>% 
  dplyr::mutate(gene = dplyr::coalesce(allele, gene_family)) %>%
  dplyr::select(-c(allele, gene_family))

# get esbl genes
esbls <- annotated_amr_data %>%
  dplyr::filter(subclass == "CEPHALOSPORIN") %>%
  dplyr::select(name, gene) %>%
  dplyr::rename(determinant = gene) %>%
  dplyr::filter(
    str_starts(determinant, "blaCTX-M") |
      str_starts(determinant, "blaSHV") |
      str_starts(determinant, "blaTEM") |
      str_starts(determinant, "blaOXA")
  )
# Parsing the point AMR data
long_point_amr_data <- point_amr_data %>%
  tidyr::pivot_longer(-name, names_to = 'metric', values_to = 'value') %>%
  tidyr::extract(col = 'metric', into = c('gene', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)
# get just assembled metric
point_assembled_metrics <- long_point_amr_data %>%
  dplyr::filter(metric == 'assembled') %>%
  dplyr::rename(assembled = value) %>%
  dplyr::select(-metric)

# find those that are assembled fully
fully_assembled_point_genes <- point_assembled_metrics  %>%
  dplyr::filter(stringr::str_starts(assembled, "yes")) %>%
  dplyr::select(-assembled)
head(fully_assembled_point_genes)

# link fully assembled genes to mutations
point_mutations <- long_point_amr_data %>%
  dplyr::filter(metric != 'assembled')
head(point_mutations)

# find those samples with point mutations in the fully assembled genes
fully_assembled_point_mutations <- fully_assembled_point_genes %>%
  dplyr::inner_join(point_mutations, by = c('name', 'gene')) %>%
  dplyr::rename(mutation = metric, mutated = value) %>%
  dplyr::filter(mutated == "yes") %>%
  dplyr::select(-mutated) %>%
  tidyr::unite(mutation, gene, mutation) # the contents of the gene column and mutation columns are merged with "_" and put within the mutation column. This process is almost opposite of tidyr::extract
head(fully_assembled_point_mutations)

# Get data about point muations for the species (E. coli)
point_mutation_metadata <- readr::read_tsv("https://gitlab.com/cgps/ghru/pipelines/data_sources/ariba_amr_databases/-/raw/master/pointfinder_db_2021-02-17/escherichia_coli_db/01.filter.check_metadata.tsv", col_names = FALSE)
# Add column names based on ariba docs
colnames(point_mutation_metadata) <- c("sequence_name", "gene?", "variant?", "mutation", "gene", "resistance")
# extract inferred resistance and name the column resistance.
# Make a column named mutation by combining the gene and existing mutation column
# Discard all the other columns
point_mutation_metadata %<>% dplyr::mutate(resistance = paste0("point_", stringr::str_match(resistance, "^Resistance: (.+?)\\.")[,2]))  %>% 
  dplyr::mutate(resistance = str_replace_all(resistance, "\\,\\s{0,4}", "\\/")) %>% 
  tidyr::unite(mutation, c(gene,mutation), sep = "_") %>%
  dplyr::select(mutation, resistance)

# PMQR
# get esbl genes
pmqr <- annotated_amr_data %>%
  dplyr::filter(subclass %in% c("QUINOLONE", "AMIKACIN/KANAMYCIN/TOBRAMYCIN/QUINOLONE")) %>%
  dplyr::select(name, gene) %>%
  dplyr::rename(determinant = gene) 
# point quinolone resistance
quinolone_specific_mutations <- point_mutation_metadata %>%
  dplyr::filter(resistance %in% c("point_Nalidixic acid/Ciprofloxacin", "point_Nalidixic acid", "point_Ciprofloxacin"))

# filter to contain mutations specific to quinolone and have an output with headers "name" and "determinant"
quinolone_point_mutations <- fully_assembled_point_mutations %>%
  dplyr::inner_join(quinolone_specific_mutations, by = "mutation") %>%
  dplyr::rename(determinant = mutation) %>%
  dplyr::mutate(determinant = stringr::str_match(determinant, "^(.+)_")[,2]) %>%
  dplyr::mutate(determinant = paste0(determinant, "-mutation")) %>%
  dplyr::distinct(name, determinant)

# filter out incomplete genes
incomplete_point_genes <- point_assembled_metrics  %>%
  filter(!stringr::str_starts(assembled, "yes"))

# quinolone_specific_point_mutation_genes <- quinolone_specific_mutations %>%
#   dplyr::mutate(gene = stringr::str_replace(mutation, "_.+$", "")) %>% # e.g in the mutation column, there is a value named ompK36_A217S. To replace the "_A217S" string with an empty space ""
#   dplyr::select(gene) %>%
#   dplyr::distinct(gene)

# incomplete_quinolone_point_genes <- incomplete_point_genes %>%
#   dplyr::inner_join(quinolone_specific_point_mutation_genes, by = "gene") %>%
#   dplyr::mutate(assembled = "incomplete") %>%
#   tidyr::unite(determinant, gene, assembled, sep = "-")

# Combining all the dataframes
all_potential_determinants <-
  rbind(pmqr, esbls, quinolone_point_mutations, incomplete_quinolone_point_genes)

all_determinant_counts <- all_potential_determinants %>%
  dplyr::count(determinant, name = "count")

# group multiple determinants per sample into a list
all_potential_determinants <-
  rbind(pmqr, esbls, quinolone_point_mutations, incomplete_quinolone_point_genes) %>%
  dplyr::group_by(name) %>%
  dplyr::summarise(determinant = list(determinant))

# Find unique determinants to become the set labels
unique_determinants <- rbind(pmqr, esbls, quinolone_point_mutations, incomplete_quinolone_point_genes) %>%
  dplyr::distinct(determinant) %>%
  dplyr::arrange(determinant) %>%
  dplyr::pull(determinant)

# make the upset plot
upset_plot <- all_potential_determinants %>%
  ggplot(aes(x=determinant)) +
  geom_bar() + theme_few() + geom_text(stat='count', aes(label=after_stat(count)), vjust=-0.5) +
  scale_x_upset(order_by = "freq", sets = unique_determinants)

# set plot width
options(repr.plot.width = 15, repr.plot.height = 10)
upset_plot

# MLST upset plot
ecoli_mlst2 <- readr::read_tsv("../../GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/MLST/combined_mlst_report.tsv", col_types = cols(.default = "c"))
# select only invasives
ecoli_mlst2 %<>% inner_join(ecoli_allRP3 %>% select(id), by = c(`Sample ID` = "id")) %>% dplyr::rename("name" = `Sample ID`)
# merge ST data with "all_potential_determinants and select the top 10
all_potential_determinants_and_ST <- all_potential_determinants %>% dplyr::left_join(ecoli_mlst2 %>% dplyr::select(name, ST), by = "name")
# top 10 ST
most_frequent_ST <- all_potential_determinants_and_ST %>% 
  dplyr::select(ST) %>% dplyr::count(ST) %>% dplyr::arrange(desc(n)) %>% 
  dplyr::slice_max(n, n = 10) %>% dplyr::pull(ST)

# Upset plot. One plot at a time
plots = list()
i <- 0
for (selected_ST in most_frequent_ST){
  i <- i + 1
  data_for_ST <- all_potential_determinants_and_ST %>% dplyr::filter(ST == selected_ST)
  head(data_for_ST)
  determinants_for_ST <- data_for_ST %>%
    unnest(cols = c(determinant)) %>%
    dplyr::distinct(determinant) %>%
    dplyr::arrange(determinant) %>%
    dplyr::pull(determinant)
  
  upset_plot_for_ST <-  data_for_ST %>%
    ggplot(aes(x = determinant)) +
    geom_bar() +
    ggtitle(paste0("ST ", selected_ST)) +
    ggeasy::easy_labs(x = "", y = "") +
    theme(plot.title = element_text(size = 20, face = "bold")) +
    scale_x_upset(order_by = "freq", sets = determinants_for_ST)
  
  plots[[i]] <- upset_plot_for_ST
}

plot_grid <- cowplot::plot_grid(plotlist = plots, align = "h") +
  cowplot::draw_figure_label(label = "determinant", position = "bottom", font = "bold") 
plot_grid

# trial
install.packages("UpSetR")
library(UpSetR)
# from https://github.com/const-ae/ggupset
all_potential_determinants %>% unnest(cols = determinant) %>% dplyr::mutate(determinant_presence=1) %>% 
  pivot_wider(names_from = determinant, values_from = determinant_presence, values_fill = list(determinant_presence = 0)) %>% as.data.frame() %>%
  upset(all_potential_determinants, sets = c("qnrVC4", "qnrS13", "blaCTX-M-27", "qepA8", "qnrS1", "qepA4", "aac(6')-Ib-cr5", "blaOXA-1", "blaCTX-M-15", "parE-mutation", "parC-mutation", "gyrA-mutation"), sets.bar.color = "black", sets.x.label = "determinant", mainbar.y.label = "Isolate counts", order.by = "freq", keep.order = TRUE)
# upset(all_potential_determinants, sets = c("gyrA-mutation", "parC-mutation", "parE-mutation", "blaCTX-M-15", "blaOXA-1", "aac(6')-Ib-cr5", "qepA4", "qnrS1", "qepA8", "blaCTX-M-27",  "qnrS13", "qnrVC4"), sets.bar.color = "black", order.by = "freq", keep.order = TRUE)
# upset(all_potential_determinants, sets = c("aac(6')-Ib-cr5", "blaCTX-M-15", "blaCTX-M-27", "blaOXA-1", "gyrA-mutation", "parC-mutation", "parE-mutation", "qepA4", "qepA8", "qnrS1", "qnrS13", "qnrVC4"), sets.bar.color = "black", order.by = "freq", empty.intersections = "on")
# Use facet wrap to make 3 plots but with different numbers of plots specified by start_indices and end_indices
# start_indices <- c(1,3,6)
# end_indices <- c(2,5,9)
# 
# plots = list()
# plot_index <- 0
# for (i in 1:3){
#   plot_index <- plot_index + 1
#   
#   start <- start_indices[i]
#   end <- end_indices[i]
#   
#   selected_STs <- most_frequent_STs[start:end]
#   data_for_STs <- all_potential_CR_determinants_and_ST %>% dplyr::filter(ST %in% selected_STs)
#   
#   determinants_for_STs <- data_for_STs %>%
#     unnest(cols = c(determinant)) %>%
#     dplyr::distinct(determinant) %>%
#     dplyr::arrange(determinant) %>%
#     dplyr::pull(determinant)
#   
#   upset_plot_for_STs <-  data_for_STs %>%
#     mutate(ST = fct_relevel(ST, selected_STs)) %>%
#     ggplot(aes(x = determinant)) +
#     geom_bar() +
#     theme(plot.title = element_text(size = 20, face = "bold")) +
#     facet_grid(cols = vars(ST), scales = "free_x", space = "free", drop = FALSE) +
#     ggeasy::easy_labs(x = "") +
#     scale_x_upset(order_by = "freq", sets = determinants_for_STs) +
#     theme(panel.spacing.x=unit(6, "lines"))
#   
#   plots[[plot_index]] <- upset_plot_for_STs
# }

# upset plot for resistance phenotypes
ecoli_AST_resistant_phenotype <- ecoli_allRP3 %>% select(c(1, 49:50, 52:53, 55:59, 61:63, 65:68)) %>% 
  pivot_longer(-id, names_to = "AMR_phenotype", values_to = "presence") %>% 
  filter(presence == "R") %>% 
  dplyr::mutate(AMR_phenotype = str_extract_all(AMR_phenotype, "^[A-Z]+", simplify = TRUE)) %>% # select only the initials of antibiotics
  dplyr::mutate(AMR_phenotype = str_replace(AMR_phenotype, "AM", "AMP"), 
                AMR_phenotype = str_replace(AMR_phenotype, "NA", "NAL"),
                AMR_phenotype = str_replace(AMR_phenotype, "GM", "GEN"),
                AMR_phenotype = str_replace(AMR_phenotype, "AN", "AMK"),
                AMR_phenotype = str_replace(AMR_phenotype, "FT", "NIT"))

ecoli_AST_resistant_phenotype %>% dplyr::select(-presence) %>% dplyr::mutate(presence=1) %>% 
  pivot_wider(names_from = AMR_phenotype, values_from = presence, values_fill = list(presence = 0)) %>% as.data.frame() %>%
  upset(all_potential_determinants, sets = c("TZP", "SXT", "SFP", "NIT",  "NAL", "IPM", "GEN", "FEP", "CXMA", "CXM", "CRO", "CIP", "AMP", "AMK"), sets.bar.color = "black", sets.x.label = "Antimicrobials (%)", mainbar.y.label = "Isolate counts", order.by = "freq", keep.order = TRUE)

# Escherichia_coli_project


## Name
Escherichia project

## Description
This theoretically should point to the scripts I used for statistics and visualization for the Escherichia paper (An ST131 clade and a phylogroup A clade bearing an O101-like O-antigen cluster predominate among bloodstream Escherichia coli isolates from South-West Nigeria hospitals). Find a link to the paper [here](https://www.microbiologyresearch.org/content/journal/mgen/10.1099/mgen.0.000863).


## Authors and acknowledgment
The Global Health Research Unit for the Genomic Surveillance of AMR Team. This work was supported by Official Development Assistance (ODA) funding from the National Institute of Health Research (grant number 16_136_111) and the Wellcome Trust grant number 206194.

## Project status
Project done.


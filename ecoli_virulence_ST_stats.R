# load libraries
library(tidyverse)
library(kableExtra)
library(janitor)
library(magrittr)
library(stats)
library(ggthemes)
library(rstatix)
library(ggpubr)

ecoli_all <- readr::read_csv("c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/microreact/ecoli_invcombov3.csv", col_types = cols(.default = "c")) %>% filter(Specimen_type == "Blood")

# Pick virulence_ST_counts_results from ecoli_virulence_ST_association.R script

# number of virulence genes per ST
virulence_ST_counts_results %>% filter(ST131 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST10 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST156 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST167 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST410 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST648 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST90 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST11025 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST12 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(`ST1642*` != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST450 != 0) %>% dplyr::pull(virulence) %>% length()
virulence_ST_counts_results %>% filter(ST942 != 0) %>% dplyr::pull(virulence) %>% length()

# virulence genes in ST10 but absent in others
virulence_ST_counts_results %>% filter(ST10 != 0 & ST131 == 0 & ST156 == 0 & ST167 == 0 & ST410 == 0 & ST648 == 0 & ST90 == 0 & ST11025 == 0 & ST12 == 0 & `ST1642*` == 0 & ST450 == 0 & ST942 == 0)
# virulence, ST, and fisher's test
# generate empty list of counts
ST131_and_yes <- c()
ST131_and_no <- c()
non_ST131_and_yes <- c()
non_ST131_and_no <- c()
virulence_names <- c()
p_values <- c()
p_signifs <- c()
lower_confidence_intervals <- c()
upper_confidence_intervals <- c()
# num_tests <- c()
# create column containing ST131 and non-ST131 rows
# ecoli_all_STnonST131add <- ecoli_all %>% filter(ST %in% c("131", "10", "156", "167", "410", "648", "90", "11025", "12", "1642*", "450", "942")) %>% 
#   mutate(STs = ifelse(ST == "131", "ST131", "Non-ST131"))

ecoli_all_STnonST131add <- ecoli_all %>%  
  mutate(STs = ifelse(ST == "131", "ST131", "Non-ST131"))

for (virulence in ecoli_virulence_genes){
  virulence_names <- c(virulence_names, virulence)
  ST131_yes <- sum(ecoli_all_STnonST131add[[virulence]] == "yes" & ecoli_all_STnonST131add$STs == "ST131", na.rm = TRUE)
  ST131_no <- sum(ecoli_all_STnonST131add[[virulence]] == "no" & ecoli_all_STnonST131add$STs == "ST131", na.rm = TRUE)
  non_ST131_yes <- sum(ecoli_all_STnonST131add[[virulence]] == "yes" & ecoli_all_STnonST131add$STs == "Non-ST131", na.rm = TRUE)
  non_ST131_no <- sum(ecoli_all_STnonST131add[[virulence]] == "no" & ecoli_all_STnonST131add$STs == "Non-ST131", na.rm = TRUE)
  
  ST131_and_yes <- c(ST131_and_yes, ST131_yes)
  ST131_and_no <- c(ST131_and_no, ST131_no)
  non_ST131_and_yes <- c(non_ST131_and_yes, non_ST131_yes)
  non_ST131_and_no <- c(non_ST131_and_no, non_ST131_no)
  
  # num_tests <- c()
  
  # Make 2x2 matrix
  contingency_tab <- as.table(matrix(c(ST131_yes, non_ST131_yes, ST131_no, non_ST131_no), nrow = 2, byrow = TRUE))
  
  # # chi square test
  # fisher_test <- contingency_tab %>% fisher.test() # I used fisher's exact because I initially saw this error "In chisq.test(.) : Chi-squared approximation may be incorrect". This means that the smallest expected frequencies is lower than 5.
  # # p-values
  # p_value <- fisher_test$p.value
  # confidence_interval <- fisher_test$conf.int
  # lower_confidence_interval <- confidence_interval[1]
  # upper_confidence_interval <- confidence_interval[2]
  
  # chi square test
  fisher_test <- contingency_tab %>% rstatix::fisher_test(detailed = TRUE, adjust_pvalue(., method = "bonferroni"))
  # p-values
  p_value <- fisher_test$p
  p_signif <- fisher_test$p.signif
  confidence_interval <- fisher_test %>% mutate(conf.int = str_c(conf.low, conf.high, sep = ", "))
  lower_confidence_interval <- fisher_test$conf.low
  upper_confidence_interval <- fisher_test$conf.high
  
  # add to list
  p_values <- c(p_values, p_value)
  p_signifs <- c(p_signifs, p_signif)
  lower_confidence_intervals <- c(lower_confidence_intervals, lower_confidence_interval)
  upper_confidence_intervals <- c(upper_confidence_intervals, upper_confidence_interval)
}

# create table
ST131non131vir_result <- tibble(
  "virulence" = virulence_names,
  "ST131_and_yes" = ST131_and_yes,
  "ST131_and_no" =  ST131_and_no,
  "Non_ST131_and_yes" = non_ST131_and_yes,
  "Non_ST131_and_no" = non_ST131_and_no,
  "Fisher's P value" = p_values,
  "Fisher's P Significance" = p_signifs,
  "Lower_Confidence_interval" = lower_confidence_intervals,
  "Upper_Confidence_interval" = upper_confidence_intervals
)
ST131non131vir_result %>% dplyr::filter(`Fisher's P Significance` != "ns") %>% view()
write.table(ST131non131vir_result, "~/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/virulence/virulenceSTnonST131fisherRstatix2.csv", sep = ",", row.names = FALSE)

# Try to continue to build bargraph of ST and virulence
STplotprep <-  ST131non131vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST131_percentage = 100 * (ST131non131vir_result$ST131_and_yes) /  (ST131non131vir_result$ST131_and_yes + ST131non131vir_result$ST131_and_no)) %>%  
  mutate(non_ST131_percentage = 100 * (ST131non131vir_result$Non_ST131_and_yes) / (ST131non131vir_result$Non_ST131_and_yes + ST131non131vir_result$Non_ST131_and_no)) %>%
  dplyr::filter(`Fisher's P Significance` != "ns") %>% dplyr::select(-`Fisher's P Significance`) %>% 
  # dplyr::select(!c(ST131_and_no, Non_ST131_and_no)) %>% 
  pivot_longer(-c(virulence, p_value, Lower_Confidence_interval, Upper_Confidence_interval, ST131_and_yes, ST131_and_no, Non_ST131_and_yes, Non_ST131_and_no), names_to = "ST", values_to = "virulence_gene_percentage") %>% 
  mutate(ST = ifelse(ST == "ST131_percentage", "ST131", "non_ST131"))

# number of virulence genes significantly more abundant in ST131 isolates compared to non-ST131 isolates
ST131non131vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST131_percentage = 100 * (ST131non131vir_result$ST131_and_yes) /  (ST131non131vir_result$ST131_and_yes + ST131non131vir_result$ST131_and_no)) %>%  
  mutate(non_ST131_percentage = 100 * (ST131non131vir_result$Non_ST131_and_yes) / (ST131non131vir_result$Non_ST131_and_yes + ST131non131vir_result$Non_ST131_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(ST131_percentage > 60) %>% dplyr::pull(virulence) %>% length()
# number of virulence genes significantly more abundant in non-ST131 isolates
ST131non131vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST131_percentage = 100 * (ST131non131vir_result$ST131_and_yes) /  (ST131non131vir_result$ST131_and_yes + ST131non131vir_result$ST131_and_no)) %>%  
  mutate(non_ST131_percentage = 100 * (ST131non131vir_result$Non_ST131_and_yes) / (ST131non131vir_result$Non_ST131_and_yes + ST131non131vir_result$Non_ST131_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(non_ST131_percentage > 25 & ST131_percentage < 75) %>% view()

# plot now
STplotprep$ST <- factor(STplotprep$ST, levels = c("ST131", "non_ST131"))

tiff(filename = "c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/draft/draft_figures_final/finally/final_figures_1-7_November2022/MGEN_ECO_Figure_3a_ST131_nonST131_virulencev2.tiff", width = 5500, height = 7500, units = 'px', res = 600)

STplotprep %>% ggplot(aes(x = virulence, y = virulence_gene_percentage, fill = ST)) + geom_bar(stat="identity", position=position_dodge()) + coord_flip() +
  facet_grid(cols =  vars(ST), space = "free_x", drop = FALSE) +
  theme_few() +
  theme(legend.position = "none",
        text = element_text(size = 20),
        axis.text.y = element_text(face = "italic")) + 
  ylab("prevalence of virulence genes (%)") + xlab("") + scale_fill_manual(values = c("black", "#969696"))
dev.off()

# virulence, ST, and fisher's test
# generate empty list of counts
ST12_and_yes <- c()
ST12_and_no <- c()
non_ST12_and_yes <- c()
non_ST12_and_no <- c()
virulence_names <- c()
p_values <- c()
lower_confidence_intervals <- c()
upper_confidence_intervals <- c()
# num_tests <- c()
# create column containing ST131 and non-ST131 rows
ecoli_all_STnonST12add <- ecoli_allRP3 %>% filter(ST %in% c("131", "10", "156", "167", "410", "648", "90", "11025", "12", "1642*", "450", "942")) %>% 
  mutate(STs = ifelse(ST == "12", "ST12", "Non_ST12"))

for (virulence in ecoli_virulence_genes){
  virulence_names <- c(virulence_names, virulence)
  ST12_yes <- sum(ecoli_all_STnonST12add[[virulence]] == "yes" & ecoli_all_STnonST12add$STs == "ST12", na.rm = TRUE)
  ST12_no <- sum(ecoli_all_STnonST12add[[virulence]] == "no" & ecoli_all_STnonST12add$STs == "ST12", na.rm = TRUE)
  non_ST12_yes <- sum(ecoli_all_STnonST12add[[virulence]] == "yes" & ecoli_all_STnonST12add$STs == "Non_ST12", na.rm = TRUE)
  non_ST12_no <- sum(ecoli_all_STnonST12add[[virulence]] == "no" & ecoli_all_STnonST12add$STs == "Non_ST12", na.rm = TRUE)
  
  ST12_and_yes <- c(ST12_and_yes, ST12_yes)
  ST12_and_no <- c(ST12_and_no, ST12_no)
  non_ST12_and_yes <- c(non_ST12_and_yes, non_ST12_yes)
  non_ST12_and_no <- c(non_ST12_and_no, non_ST12_no)
  
  # num_tests <- c()
  
  # Make 2x2 matrix
  contingency_tab <- as.table(matrix(c(ST12_yes, non_ST12_yes, ST12_no, non_ST12_no), nrow = 2, byrow = TRUE))
  
  
  # chi square test
  fisher_test <- contingency_tab %>% fisher.test() # I used fisher's exact because I initially saw this error "In chisq.test(.) : Chi-squared approximation may be incorrect". This means that the smallest expected frequencies is lower than 5.
  # p-values
  p_value <- fisher_test$p.value
  confidence_interval <- fisher_test$conf.int
  lower_confidence_interval <- confidence_interval[1]
  upper_confidence_interval <- confidence_interval[2]
  
  # add to list
  p_values <- c(p_values, p_value)
  lower_confidence_intervals <- c(lower_confidence_intervals, lower_confidence_interval)
  upper_confidence_intervals <- c(upper_confidence_intervals, upper_confidence_interval)
}

# create table
ST12non12vir_result <- tibble(
  "virulence" = virulence_names,
  "ST12_and_yes" = ST12_and_yes,
  "ST12_and_no" =  ST12_and_no,
  "Non_ST12_and_yes" = non_ST12_and_yes,
  "Non_ST12_and_no" = non_ST12_and_no,
  "Fisher's P value" = p_values,
  "Lower_Confidence_interval" = lower_confidence_intervals,
  "Upper_Confidence_interval" = upper_confidence_intervals
)
ST12non12vir_result

# Try to continue to build bargraph of ST and virulence
ST12plotprep <-  ST12non12vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST12_percentage = 100 * (ST12non12vir_result$ST12_and_yes) /  (ST12non12vir_result$ST12_and_yes + ST12non12vir_result$ST12_and_no)) %>%  
  mutate(non_ST12_percentage = 100 * (ST12non12vir_result$Non_ST12_and_yes) / (ST12non12vir_result$Non_ST12_and_yes + ST12non12vir_result$Non_ST12_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% 
  pivot_longer(-c(virulence, p_value, Lower_Confidence_interval, Upper_Confidence_interval, ST12_and_yes, ST12_and_no, Non_ST12_and_yes, Non_ST12_and_no), names_to = "ST", values_to = "virulence_gene_percentage") %>% 
  mutate(ST = ifelse(ST == "ST12_percentage", "ST12", "non_ST12"))

# number of virulence genes significantly more abundant in ST12 isolates compared to non-ST12 isolates
ST12non12vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST12_percentage = 100 * (ST12non12vir_result$ST12_and_yes) /  (ST12non12vir_result$ST12_and_yes + ST12non12vir_result$ST12_and_no)) %>%  
  mutate(non_ST12_percentage = 100 * (ST12non12vir_result$Non_ST12_and_yes) / (ST12non12vir_result$Non_ST12_and_yes + ST12non12vir_result$Non_ST12_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(ST12_percentage > 60) %>% dplyr::pull(virulence) %>% length()
# number of virulence genes significantly more abundant in non-ST131 isolates
ST12non12vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST12_percentage = 100 * (ST12non12vir_result$ST12_and_yes) /  (ST12non12vir_result$ST12_and_yes + ST12non12vir_result$ST12_and_no)) %>%  
  mutate(non_ST12_percentage = 100 * (ST12non12vir_result$Non_ST12_and_yes) / (ST12non12vir_result$Non_ST12_and_yes + ST12non12vir_result$Non_ST12_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(non_ST12_percentage > 25 & ST12_percentage < 75) %>% view()
# plot now
ST12plotprep$ST <- factor(ST12plotprep$ST, levels = c("ST12", "non_ST12"))
ST12plotprep %>% ggplot(aes(x = virulence, y = virulence_gene_percentage, fill = ST)) + geom_bar(stat="identity", position=position_dodge()) + coord_flip() +
  facet_grid(cols =  vars(ST), space = "free_x", drop = FALSE) +
  theme_few() +
  theme(legend.position = "none") + 
  ylab("prevalence of virulence genes (%)") + xlab("") + scale_fill_manual(values = c("black", "#969696"))



# Onovel32 clade calculation and plots
# virulence, ST, and fisher's test
# generate empty list of counts
Onovel32_and_yes <- c()
Onovel32_and_no <- c()
non_Onovel32_and_yes <- c()
non_Onovel32_and_no <- c()
virulence_names <- c()
p_values <- c()
p_signifs <- c()
lower_confidence_intervals <- c()
upper_confidence_intervals <- c()
# num_tests <- c()
# create column containing Onovel32 and non-Onovel32 rows
ecoli_all_Onovel32nonOnovel32add <- ecoli_all %>%  
  mutate(Onovel32s = ifelse(id %in% c("G18484079", "G18503408", "G18584158", "G18584159", "G18584161", "G18584163", "G18600004", "G18600026", "G18600033", "G18600048", "GI8584164"), "Onovel32_clade", "Non_Onovel32_clade"))

for (virulence in ecoli_virulence_genes){
  virulence_names <- c(virulence_names, virulence)
  Onovel32_yes <- sum(ecoli_all_Onovel32nonOnovel32add[[virulence]] == "yes" & ecoli_all_Onovel32nonOnovel32add$Onovel32s == "Onovel32_clade", na.rm = TRUE)
  Onovel32_no <- sum(ecoli_all_Onovel32nonOnovel32add[[virulence]] == "no" & ecoli_all_Onovel32nonOnovel32add$Onovel32s == "Onovel32_clade", na.rm = TRUE)
  non_Onovel32_yes <- sum(ecoli_all_Onovel32nonOnovel32add[[virulence]] == "yes" & ecoli_all_Onovel32nonOnovel32add$Onovel32s == "Non_Onovel32_clade", na.rm = TRUE)
  non_Onovel32_no <- sum(ecoli_all_Onovel32nonOnovel32add[[virulence]] == "no" & ecoli_all_Onovel32nonOnovel32add$Onovel32s == "Non_Onovel32_clade", na.rm = TRUE)
  
  Onovel32_and_yes <- c(Onovel32_and_yes, Onovel32_yes)
  Onovel32_and_no <- c(Onovel32_and_no, Onovel32_no)
  non_Onovel32_and_yes <- c(non_Onovel32_and_yes, non_Onovel32_yes)
  non_Onovel32_and_no <- c(non_Onovel32_and_no, non_Onovel32_no)
  
  # num_tests <- c()
  
  # Make 2x2 matrix
  contingency_tab <- as.table(matrix(c(Onovel32_yes, non_Onovel32_yes, Onovel32_no, non_Onovel32_no), nrow = 2, byrow = TRUE))
  
  
  # chi square test
  fisher_test <- contingency_tab %>% rstatix::fisher_test(detailed = TRUE, adjust_pvalue(., method = "bonferroni"))
  # p-values
  p_value <- fisher_test$p
  p_signif <- fisher_test$p.signif
  confidence_interval <- fisher_test %>% mutate(conf.int = str_c(conf.low, conf.high, sep = ", "))
  lower_confidence_interval <- fisher_test$conf.low
  upper_confidence_interval <- fisher_test$conf.high
  
  # add to list
  p_values <- c(p_values, p_value)
  p_signifs <- c(p_signifs, p_signif)
  lower_confidence_intervals <- c(lower_confidence_intervals, lower_confidence_interval)
  upper_confidence_intervals <- c(upper_confidence_intervals, upper_confidence_interval)
}

# create table
Onovel32nonOnovel32vir_result <- tibble(
  "virulence" = virulence_names,
  "Onovel32_and_yes" = Onovel32_and_yes,
  "Onovel32_and_no" =  Onovel32_and_no,
  "Non_Onovel32_and_yes" = non_Onovel32_and_yes,
  "Non_Onovel32_and_no" = non_Onovel32_and_no,
  "Fisher's P value" = p_values,
  "Fisher's P Significance" = p_signifs,
  "Lower_Confidence_interval" = lower_confidence_intervals,
  "Upper_Confidence_interval" = upper_confidence_intervals
)
Onovel32nonOnovel32vir_result %>% view()
write.table(Onovel32nonOnovel32vir_result, "c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/virulence/virulenceOnovel32nonOnovel32fisher.csv", sep = ",", row.names = FALSE)

# Try to continue to build bargraph of Onovel32 and virulence
Onovel32plotprep <-  Onovel32nonOnovel32vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(Onovel32_percentage = 100 * (Onovel32nonOnovel32vir_result$Onovel32_and_yes) /  (Onovel32nonOnovel32vir_result$Onovel32_and_yes + Onovel32nonOnovel32vir_result$Onovel32_and_no)) %>%  
  mutate(non_Onovel32_percentage = 100 * (Onovel32nonOnovel32vir_result$Non_Onovel32_and_yes) / (Onovel32nonOnovel32vir_result$Non_Onovel32_and_yes + Onovel32nonOnovel32vir_result$Non_Onovel32_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% dplyr::select(-`Fisher's P Significance`) %>% 
  # dplyr::select(!c(Onovel32_and_no, Non_Onovel32_and_no)) %>% 
  pivot_longer(-c(virulence, p_value, Lower_Confidence_interval, Upper_Confidence_interval, Onovel32_and_yes, Onovel32_and_no, Non_Onovel32_and_yes, Non_Onovel32_and_no), names_to = "Onovel32", values_to = "virulence_gene_percentage") %>% 
  mutate(Onovel32 = ifelse(Onovel32 == "Onovel32_percentage", "Onovel32_clade", "non_Onovel32_clade"))

# number of virulence genes significantly more abundant in Onovel32 isolates compared to non-Onovel32 isolates
Onovel32nonOnovel32vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(Onovel32_percentage = 100 * (Onovel32nonOnovel32vir_result$Onovel32_and_yes) /  (Onovel32nonOnovel32vir_result$Onovel32_and_yes + Onovel32nonOnovel32vir_result$Onovel32_and_no)) %>%  
  mutate(non_Onovel32_percentage = 100 * (Onovel32nonOnovel32vir_result$Non_Onovel32_and_yes) / (Onovel32nonOnovel32vir_result$Non_Onovel32_and_yes + Onovel32nonOnovel32vir_result$Non_Onovel32_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(Onovel32_percentage > 60) %>% dplyr::pull(virulence) %>% length()
# number of virulence genes significantly more abundant in non-Onovel32 isolates
ST131non131vir_result %>% dplyr::rename("p_value" = "Fisher's P value") %>% 
  mutate(ST131_percentage = 100 * (ST131non131vir_result$ST131_and_yes) /  (ST131non131vir_result$ST131_and_yes + ST131non131vir_result$ST131_and_no)) %>%  
  mutate(non_ST131_percentage = 100 * (ST131non131vir_result$Non_ST131_and_yes) / (ST131non131vir_result$Non_ST131_and_yes + ST131non131vir_result$Non_ST131_and_no)) %>%
  dplyr::filter(p_value < 0.05) %>% filter(non_ST131_percentage > 25 & ST131_percentage < 75) %>% dplyr::pull(virulence) %>% length()

# plot now
Onovel32plotprep$Onovel32 <- factor(Onovel32plotprep$Onovel32, levels = c("Onovel32_clade", "non_Onovel32_clade"))

tiff(filename = "c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/draft/draft_figures_final/MGEN_ECO_Figure_4a_Onovel32_non_Onovel32_virulencev2.tiff", width = 2000, height = 2200, units = 'px', res = 300)

Onovel32plotprep %>% ggplot(aes(x = virulence, y = virulence_gene_percentage, fill = Onovel32)) + geom_bar(stat="identity", position=position_dodge()) + coord_flip() +
  facet_grid(cols =  vars(Onovel32), space = "free_x", drop = FALSE) +
  theme_few() +
  theme(legend.position = "none",
        text = element_text(size = 20),
        axis.text.y = element_text(face = "italic")) + 
  ylab("prevalence of virulence genes (%)") + xlab("") + scale_fill_manual(values = c("black", "#969696"))
dev.off()


# point out cpsACP and ugd in plot
tiff(filename = "c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/draft/draft_figures_final/finally/final_figures_1-7_November2022/MGEN_ECO_Figure_4a_Onovel32_non_Onovel32_virulencev2.tiff", width = 6000, height = 8000, units = 'px', res = 600)

Onovel32plotprep %>% mutate(cpsACPugd = case_when(!virulence %in% c("cpsACP", "ugd") ~ "non_cpsACP_ugd",
                                                  virulence == "cpsACP" ~ "cpsACP",
                                                  virulence == "ugd" ~ "ugd")) %>%  
  ggplot(aes(x = virulence, y = virulence_gene_percentage, fill = cpsACPugd)) + geom_bar(stat="identity", position=position_dodge()) + coord_flip() +
  facet_grid(cols =  vars(Onovel32), space = "free_x", drop = FALSE) +
  theme_few() +
  theme(legend.position = "none",
        text = element_text(size = 20),
        axis.text.y = element_text(face = "italic")) + 
  ylab("prevalence of virulence genes (%)") + xlab("") + scale_fill_manual(values = c("red", "#969696", "magenta"))

dev.off()

library(tidyverse)
library(kableExtra)
library(janitor)
library(magrittr)

## unrelated, but helps in renaming columns without the need to pivot long
# https://blog.exploratory.io/renaming-column-names-for-multiple-columns-together-9d216e37bf41

CDEC_ecoli_mlst <- readr::read_tsv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/MLST/combined_mlst_report.tsv", col_types = cols(.default = "c"))
# get the index of name of column called "known_variants"
known_variants <- which(names(readr::read_csv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/AMR_report/combined_summary.csv", col_types = cols(.default = "c"))) == 'known_variants')
CDEC_acquired_amr_data <- readr::read_csv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/AMR_report/combined_summary.csv", col_types = cols(.default = "c")) %>% select(c(1:known_variants - 1))
CDEC_point_amr_data <- readr::read_csv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/AMR_report/combined_summary.csv", col_types = cols(.default = "c")) %>% select(c(1, (known_variants + 1):ncol(.))) 
CDEC_ecoli_plasmid <- readr::read_csv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/plasmid_report/ariba_plasmidfinder_summary.csv", col_types = cols(.default = "c"))
CDEC_ecoli_virulence <- readr::read_csv("/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/virulence_reports/re_analysis/json/myvirulence_finder_summary.csv", col_types = cols(.default = "c"))
# epidemiological_data <- readr::read_csv("../../GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Klebsiella_pneumoniae/Epidemiological_data/NG_updated_retrospective_metadata_15092020.csv", col_types = cols(.default = "c"))

# Get meta data about acquired genes from the NCBI AMR gene catalogue
ncbi_acquired_gene_metadata <- readr::read_tsv("https://ftp.ncbi.nlm.nih.gov/pathogen/Antimicrobial_resistance/Data/latest/ReferenceGeneCatalog.txt")
head(ncbi_acquired_gene_metadata) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

# Get data about point mutations for the species
# point_mutation_metadata <- readr::read_tsv("https://gitlab.com/cgps/ghru/pipelines/data_sources/ariba_amr_databases/-/raw/master/pointfinder/klebsiella_db/01.filter.check_metadata.tsv", col_names = FALSE)
point_mutation_metadata <- readr::read_tsv("https://gitlab.com/cgps/ghru/pipelines/data_sources/ariba_amr_databases/-/raw/master/pointfinder_db_2021-02-17/escherichia_coli_db/01.filter.check_metadata.tsv", col_names = FALSE)

# Add column names based on ariba docs
colnames(point_mutation_metadata) <- c("sequence_name", "gene?", "variant?", "mutation", "gene", "resistance")
# extract inferred resistance and name the column resistance.
# Make a column named mutation by combining the gene and existing mutation column
# Discard all the other columns
point_mutation_metadata %<>% dplyr::mutate(resistance = paste0("point_", stringr::str_match(resistance, "^Resistance: (.+?)\\.")[,2]))  %>% 
  tidyr::unite(mutation, c(gene,mutation), sep = "_") %>% 
  dplyr::select(mutation, resistance)
head(point_mutation_metadata) %>% kable() %>% kable_styling()

# make data long so that for each gene there is a single row for each observation of a gene in a gene cluster
long_amr_data <- CDEC_acquired_amr_data %>%
  tidyr::pivot_longer(-name, names_to = 'metric', values_to = 'value') %>%
  tidyr::extract(col = 'metric', into = c('ariba_cluster', 'metric'), regex = '^([^\\.]+)\\.(.+)', remove = TRUE)%>%
  tidyr::pivot_wider(names_from = 'metric', values_from = 'value')

# filter out only good results based on assembly status (starts with yes),convert ctg_cov to float and filter for depth of coverage > 10
filtered_long_amr_data <- long_amr_data %>%
  filter(stringr::str_starts(assembled, "yes"))

filtered_long_amr_data  %<>% na_if("NA") %>%
  mutate(ctg_cov = as.numeric(ctg_cov)) %>%
  filter(ctg_cov > 10)

# select only Sample id and accession columns and extract just the accession from the ref_seq column, renaming it to refseq_nucleotide_accession
filtered_long_amr_data  %<>%
  select(name, ref_seq) %>%
  mutate(ref_seq = stringr::str_extract(ref_seq, "NG_.+$")) %>%
  rename(refseq_nucleotide_accession = ref_seq)

# select only the required columns from the NCBI metadata
ncbi_acquired_gene_metadata  %<>% 
  filter(type == "AMR") %>% 
  select(refseq_nucleotide_accession, allele, gene_family, class,subclass) %>% 
  drop_na(allele) %>%
  drop_na(gene_family) %>% 
  drop_na(refseq_nucleotide_accession)

# combine amr data with NCBI metadata using the refseq_nucleotide_accession to join the tables
annotated_acquired_amr_data <- filtered_long_amr_data %>%
  left_join(ncbi_acquired_gene_metadata, by = "refseq_nucleotide_accession")

# Some deyerminants do not have alleles and therefore the allele and gene_family should be merged into one column where allele does not exist. THe coalesce function can be used for this
annotated_acquired_amr_data %<>% dplyr::mutate(gene = dplyr::coalesce(allele, gene_family)) %>%
  select(-c(allele, gene_family))

head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

# For the acquired genes the NCBI subclasses have many different variants and some should be rationalised. A suggestion would be to use an external table as a source for combining.
# Get the subclass groups from the Google sheets
subclass_groups <- googlesheets4::read_sheet("https://docs.google.com/spreadsheets/d/1X4-xD2-e78geJOFLOKri2QjeOzhrPLHzwsAN_f8mX1Q")

# use this to merge subclasses
annotated_acquired_amr_data  %<>% 
  dplyr::left_join(subclass_groups %>% select(-class), by = "subclass") %>%
  dplyr::select(-subclass) %>%
  dplyr::rename(subclass = grouped_subclass)
head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

# Sometimes the class doesn’t have a subclass. Therefore these columns should be combined using the coalesce funtion again.
annotated_acquired_amr_data %<>% dplyr::mutate(class = dplyr::coalesce(subclass, class)) %>% 
  select(name,gene,class)
head(annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

## just a small digression to filter out rows with NA
annotated_acquired_amr_data %<>% filter(!is.na(class))
# Many samples will have more than one determinant for some classes. These should be combined before returning the data frame to wide format of one row per sample.

# In the final data we will want something for every sample even if it’s empty. This is the purpose of the final left join

# Use group_by and summarise functions to collapse multiple occurences of genes in the same class
annotated_acquired_amr_data %<>% dplyr::group_by(name, class) %>%
  dplyr::summarise(gene = paste0(gene, collapse = ','))

# pivot this data back to wide format
wide_annotated_acquired_amr_data <- annotated_acquired_amr_data %>% 
  pivot_wider(names_from = c(class), values_from =c (gene))

# ensure all samples have data
wide_annotated_acquired_amr_data <- CDEC_acquired_amr_data %>% dplyr::select(name) %>% 
  dplyr::left_join(wide_annotated_acquired_amr_data, by = "name")

head(wide_annotated_acquired_amr_data) %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")

# add point mutation metadata
CDEC_acquired_point_amr <- wide_annotated_acquired_amr_data %>% left_join(CDEC_point_amr_data %>% select(name, parC.T57S), by = "name") %>% rename(POINT_QUINOLONE = parC.T57S) %>%  mutate(POINT_QUINOLONE =  if_else(POINT_QUINOLONE == "yes", "parC.T57S", "-"))
# adding uncollapsed annotated_acquired_amr_data
CDEC_acquired_point_amr_genes <- annotated_acquired_amr_data %>% filter(!is.na(gene)) %>% select(-class) %>% mutate(presence = "present") %>% 
  pivot_wider(names_from = gene, values_from = presence) %>% pivot_longer(-name, names_to = "gene", values_to = "presence") %>% 
  mutate(presence = if_else(is.na(presence), "absent", "present")) %>%  pivot_wider(names_from = gene, values_from = presence)
# add both data
CDEC_amr_combo <- CDEC_acquired_point_amr %>% left_join(CDEC_acquired_point_amr_genes, by = "name") %>% mutate(parC.T57S = if_else(POINT_QUINOLONE == "parC.T57S", "present", "absent"))

# sare plot
## antibiotic subclass
CDEC_amr_subclass_counts <- CDEC_amr_combo %>% select(1:10) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
  drop_na(value) %>% filter(value != "-") %>% count(metric, name = "metric_count") %>% slice_max(metric_count, n = nrow(.)) 
# plot
CDEC_amr_combo %>% select(1:10) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
  drop_na(value) %>%   filter(value != "-") %>%
  mutate(metric = fct_relevel(metric, CDEC_amr_subclass_counts$metric)) %>% 
  ggplot(aes(x=metric)) + geom_bar(aes(fill = name)) + scale_fill_manual(values = antibiotics_colours)  + 
  theme_few() + geom_text(stat='count', aes(label=after_stat(count)), vjust=-0.5) + 
  ggeasy::easy_rotate_x_labels(angle=270) + ggeasy::easy_move_legend("bottom")

## AMR genes
CDEC_amr_gene_counts <- CDEC_amr_combo %>% select(1, 11:ncol(.)) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
  drop_na(value) %>% filter(value != "absent") %>% count(metric, name = "metric_count") %>% slice_max(metric_count, n = nrow(.)) 
# plot
CDEC_amr_combo %>% select(1, 11:ncol(.)) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
  drop_na(value) %>% filter(value != "absent") %>%  mutate(metric = fct_relevel(metric, CDEC_amr_gene_counts$metric)) %>% 
  left_join(annotated_acquired_amr_data %>% rename("metric"=gene), by = c("name", "metric")) %>% mutate(class = replace_na(class, "POINT-QUINOLONE")) %>% 
  ggplot(aes(x=metric)) + geom_bar(aes(fill = name)) + facet_wrap(vars(class), ncol = 3, scales = "free_y") + scale_fill_manual(values = antibiotics_colours) + coord_flip() + 
  theme_few() + geom_text(stat='count', aes(label=after_stat(count)), vjust=-0.5) +
  ggeasy::easy_rotate_x_labels(angle=270) + ggeasy::easy_move_legend("bottom") + ggplot2::guides(fill = guide_legend(ncol = 8))

# colour subclass
CDEC_amr_subclass_colour <- CDEC_amr_combo %>% select(1:10) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
  mutate(value = str_replace_na(value, "-")) %>%  mutate(colour = if_else(value == "-", "white", "red")) %>% 
  mutate(metric_colour = str_c(metric, "__colour")) %>% select(-c(metric, value)) %>% pivot_wider(names_from = metric_colour, values_from = colour)
# join with subclass data
CDEC_acquired_amr_data_n_colour <-  CDEC_amr_combo %>% select(1:10) %>% left_join(CDEC_amr_subclass_colour, by = "name")
# colour AMR genes
CDEC_amr_gene_colour <-  CDEC_amr_combo %>% select(1, 11:ncol(.)) %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>%
  mutate(colour = if_else(value == "present", "red", "white")) %>% mutate(metric_colour = str_c(metric, "__colour")) %>% select(-c(metric, value)) %>% 
  pivot_wider(names_from = metric_colour, values_from = colour) %>%  select(name, all_of(sort(setdiff(colnames(.), c("name")))))
# join with AMR genes
CDEC_amr_gene_data_n_colour <- CDEC_amr_combo %>% select(1, 11:ncol(.)) %>% select(name, all_of(sort(setdiff(colnames(.), c("name"))))) %>% left_join(CDEC_amr_gene_colour, by = "name")
# join subclass data 'n' colour with amr gene data 'n' colour
CDEC_amr_combo_n_colour <- CDEC_acquired_amr_data_n_colour %>% left_join(CDEC_amr_gene_data_n_colour, by = "name")

# work on the colours for MLST, virulence, and plasmid
# mlst
CDEC_ecoli_mlst2 <- CDEC_ecoli_mlst %>% dplyr::rename("name" = "Sample ID") %>% dplyr::select(name, ST) %>% dplyr::mutate(ST__autocolour = ST)
# virulence
CDEC_ecoli_virulence2 <- CDEC_ecoli_virulence %>% dplyr::rename("name" = "Samples") %>% dplyr::left_join(CDEC_ecoli_virulence %>% dplyr::rename("name" = "Samples") %>%  
                                                                                                           tidyr::pivot_longer(-name, names_to = "metric", values_to = "value") %>% 
                                                                                                           dplyr::mutate(metric__colour = paste0(metric, "__colour")) %>% 
                                                                                                           dplyr::mutate(colour = dplyr::if_else(value == "yes", "red", "white")) %>% 
                                                                                                           dplyr::select(name, metric__colour, colour) %>% 
                                                                                                           tidyr::pivot_wider(names_from = metric__colour, values_from = colour), by = "name") %>% 
  select(name, all_of(sort(setdiff(colnames(.), c("name")))))
# plasmid
CDEC_ecoli_plasmid %<>% left_join(CDEC_ecoli_plasmid %>% pivot_longer(-name, names_to = "metric", values_to = "value") %>% mutate(metric__colour = str_c(metric, "__colour"),
                                                                                                                                 colour = if_else(value == "yes", "red", "white")) %>% 
                                   select(name, metric__colour, colour) %>% pivot_wider(names_from = metric__colour, values_from = colour), by = "name") 
######## merge all data together #####################
combined_coloured_data <- CDEC_ecoli_mlst2 %>% left_join(CDEC_amr_combo_n_colour, by = "name") %>% left_join(CDEC_ecoli_virulence2, by = "name") %>%
  left_join(CDEC_ecoli_plasmid, by = "name")

# save
write.table(combined_coloured_data, "/Users/ghru/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/microreact/CDEC.csv", sep = ",", row.names = FALSE)

# from windows. Notes for methodology. Virulence using GHRU pipeline, ST and Serotype using srst2 and CGE MLST
CDEC_virulence <- readr::read_csv("c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/virulence_reports/new_analysis_minus_blood/ariba_vfdb_core_summary.csv")
CDEC_virulence2 <- CDEC_virulence %>% dplyr::rename("id" = "name") %>% dplyr::left_join(CDEC_virulence %>% dplyr::rename("id" = "name") %>%  
                                                                                                           tidyr::pivot_longer(-id, names_to = "metric", values_to = "value") %>% 
                                                                                                           dplyr::mutate(metric__colour = paste0(metric, "__colour")) %>% 
                                                                                                           dplyr::mutate(colour = dplyr::if_else(value == "yes", "red", "white")) %>% 
                                                                                                           dplyr::select(id, metric__colour, colour) %>% 
                                                                                                           tidyr::pivot_wider(names_from = metric__colour, values_from = colour), by = "id") %>% 
  select(id, all_of(sort(setdiff(colnames(.), c("id")))))

write.table(CDEC_virulence2, "c:/Users/Afolayan/Documents/GHRU_stuffs/GHRU_analysis/GHRU_retrospective_2018/Escherichia_coli/CDEC/virulence_reports/new_analysis_minus_blood/CDEC_microreact.csv", sep = ",", row.names = FALSE)
